class CreatePetitions < ActiveRecord::Migration[6.0]
  def change
    create_table :petitions do |t|
      t.references  :creator, references: :user
      t.references  :approver, references: :user
      t.string      :title
      t.text        :content
      t.integer     :break_time, limit: 1, default: 3
      t.date        :day_off
      t.timestamps
    end
  end
end

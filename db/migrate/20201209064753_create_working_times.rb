class CreateWorkingTimes < ActiveRecord::Migration[6.0]
  def change
    create_table :working_times do |t|
      t.belongs_to :user, class_name: "User", foreign_key: "user_id"
      t.date     :working_date
      t.datetime :check_in
      t.datetime :check_out
      t.integer  :workdays
      t.timestamps
    end
  end
end

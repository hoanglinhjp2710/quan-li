class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.belongs_to :user, class_name: "User", foreign_key: "user_id"
      t.belongs_to :post_type
      t.string :title
      t.string :content
      t.string :user_confirm
      t.timestamps
    end
  end
end

class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.belongs_to :user
      t.string  :first_name
      t.string  :last_name
      t.date    :birthdate
      t.integer :gender , limit: 1, default: 2
      t.string  :address
      t.string  :phone_number
      t.integer :salary
      t.timestamps
    end
  end
end

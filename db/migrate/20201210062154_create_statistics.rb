class CreateStatistics < ActiveRecord::Migration[6.0]
  def change
    create_table :statistics do |t|
      t.belongs_to  :user
      t.integer     :paid_leave_total
      t.integer     :on_leave_total

      t.timestamps
    end
  end
end

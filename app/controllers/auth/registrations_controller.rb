class Auth::RegistrationsController < BaseController
  # skip_before_action :authenticate!

  def new
    @user = User.new
  end

  def create
      service.create(parameters.creating_params)
      flash[:success] = I18n.t('.registrations.success')
      redirect_to new_auth_session_path
    rescue Exception => e
      flash[:error] = "error: #{e.message}"
      redirect_to new_auth_registration_path
  end
  def forgot_password
  end

  private
  def service
    service ||= Auth::RegistrationsService.new
  end

  def parameters
    parameters ||= Auth::RegistrationsParams.new params
  end
end

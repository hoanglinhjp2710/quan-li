class Auth::SessionsController < BaseController

  def new
    @user = User.new
  end

  def create
    user = warden.authenticate! :password
    flash[:success] = I18n.t('.sign_in.success')
    redirect_to record_staff_management_index_path(locale: :vi)
  end

  def destroy
    warden.logout
    redirect_to new_auth_session_path
  end
end

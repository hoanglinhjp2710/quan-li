class Record::ProfilesController < DashboardController
  before_action :find_profile, only: [:edit, :update, :show]
  def new;  end

  def create
    service.create_profile
    flash[:success] = I18n.t('.profile.create.success')
    redirect_to record_profile_path
  end

  def edit; end

  def update
    service.update_profile(@profile)
    flash[:success] = I18n.t('profile.update.success')
    redirect_to record_profile_path
  end

  def show; end

  def profile_setting
    @profile = Profile.find_by(user_id: current_user.id)
    if @profile.nil?
      @profile = Profile.new
      render 'new'
    else
      render 'edit'
    end
  end
  private

  def find_profile
    @profile = Profile.find_by(user_id: current_user.id)
  end

  def parameters
    parameters ||= Record::ProfileParams.new params
  end

  def service
    service ||= Record::ProfileService.new(parameters, current_user)
  end
end

module Record
  class ProfileService < BaseService
    def initialize(params, user)
      @params = params
      @user = user
    end

    def update_profile(profile)
      profile.update(@params.updating_params)
    end

    def create_profile
      @profile = Profile.find_by(user_id: @user.id)
      if @profile.present?
        @profile.update!(@params.creating_params)
      else
        @profile = Profile.new (@params.creating_params.merge(user_id: @user.id))
        @profile.save!
      end
    end

    def find_profile
      @profile = Profile.find_by_user_id(user_id: @user.id)
    end
  end
end
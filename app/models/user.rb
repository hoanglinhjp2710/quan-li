class User < ApplicationRecord

  has_secure_password

  enum role: { employee: 0, manager: 1, admin: 2 }

  validates :username, presence: true, uniqueness: { case_sensitive: false }, length: { in: 6..20 }
  validates :password, presence: true, length: { in: 6...20}
  validates :email, presence: true, uniqueness: { case_sensitive: false },
                    format: { with: Settings.regex.email }

end

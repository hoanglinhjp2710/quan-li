class WorkingTime < ApplicationRecord
  belongs_to :user, class_name: "User", foreign_key: "user_id"

  validates :working_date, presence: true
  validates :check_in, presence: true
  validates :check_out, presence: true
  validates :workdays, presence: true

end

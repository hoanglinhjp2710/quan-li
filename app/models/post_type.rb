class PostType < ApplicationRecord

  validates :name, presence: true
end

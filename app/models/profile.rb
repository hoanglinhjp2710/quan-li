class Profile < ApplicationRecord
  belongs_to :user, class_name: "User", foreign_key: "user_id"

  has_one_attached :avatar

  enum gender: { male: 0, fmale: 1, other: 2 }

  validates :phone_number , presence: true, format: { with: Settings.regex.number_phone }
  validates :first_name, presence: true, length: { in: 2...20 }
  validates :last_name, presence: true, length: { in: 2...20 }
  validates :address, presence: true, length: { in: 10...50 }
  validates :birthdate, presence: true
end

class Post < ApplicationRecord
  has_many_attached :image
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :post_type
end

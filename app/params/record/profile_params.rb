module Record
  class ProfileParams < BaseParams
    def creating_params
      params.require(:profile).permit(:first_name, :last_name, :birthdate, :gender, :address, :phone_number, :avatar)
    end

    def updating_params
      params.require(:profile).permit(:first_name, :last_name, :birthdate, :gender, :address, :phone_number, :avatar)
    end
  end
end
module Auth
  class RegistrationsParams < BaseParams
    def creating_params
      params.require(:user).permit(:username, :email, :password)
    end

    def authentication_token
      params[:authenticity_token]
    end
  end
end
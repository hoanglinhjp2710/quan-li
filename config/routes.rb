Rails.application.routes.draw do
  scope '(:locale)', locale: /en|vi/ do
    namespace :auth do
      resource :registration do
        collection do
          get 'forgot_password', to: 'registrations#forgot_password'
        end
      end
      resources :sessions, only: %i[new create]
      delete '/logout', to: 'sessions#destroy'
    end
    namespace :record do
      resources :staff_management, only: [:index]
      resource :profile
      get 'profile/setting', to: 'profiles#profile_setting'
    end
  end
end
